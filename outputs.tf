output "id" {
  value       = azurerm_redis_cache.this.id
  description = "Redis instance identifier"
}

output "hostname" {
  value       = azurerm_redis_cache.this.hostname
  description = "Redis instance hostname"
}

output "ssl_port" {
  value       = azurerm_redis_cache.this.ssl_port
  description = "Redis instance SSL port"
}

output "port" {
  value       = azurerm_redis_cache.this.port
  description = "Redis instance port"
}

output "primary_access_key" {
  value       = azurerm_redis_cache.this.primary_access_key
  description = "Redis instance primary access key"
}

output "secondary_access_key" {
  value       = azurerm_redis_cache.this.secondary_access_key
  description = "Redis instance secondary access key"
}

output "primary_connection_string" {
  value       = azurerm_redis_cache.this.primary_connection_string
  description = "Redis instance primary connection string"
}

output "secondary_connection_string" {
  value       = azurerm_redis_cache.this.secondary_connection_string
  description = "Redis instance secondary connection string"
}
