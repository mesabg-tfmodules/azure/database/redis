variable "name" {
  type        = string
  description = "Project name"
}

variable "environment" {
  type        = string
  description = "Environment name"
  default     = "develop"
}

variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "redis_version" {
  type        = string
  description = "Redis version"
  default     = "6"
}

variable "capacity" {
  type        = number
  description = "Cluster capacity units"
  default     = 1
}

variable "family" {
  type        = string
  description = "Cluster capacity family"
  default     = "C"
}

variable "sku_name" {
  type        = string
  description = "Redis SKU name"
  default     = "Basic"  
}

variable "minimum_tls_version" {
  type        = string
  description = "Minimum TLS version"
  default     = "1.2"
}

variable "public_network_access_enabled" {
  type        = bool
  description = "Enable public access"
  default     = false
}

variable "non_ssl_port_enabled" {
  type        = bool
  description = "Enable non SSL port"
  default     = false
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
