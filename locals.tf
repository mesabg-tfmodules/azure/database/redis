locals {
  tags          = merge(var.tags, {
    source      = "terraform"
    environment = var.environment
  })
}
